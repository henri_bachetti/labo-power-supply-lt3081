EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lt3081
LIBS:JQC-3F
LIBS:ina219-module
LIBS:arduino
LIBS:+20v
LIBS:mcp1702
LIBS:tl431
LIBS:regulator-LT3081-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X02 P1
U 1 1 58A374A4
P 3450 2550
F 0 "P1" H 3450 2700 50  0000 C CNN
F 1 "INPUT" V 3550 2550 50  0000 C CNN
F 2 "myConnectors:JST-B2P-VH" H 3450 2550 50  0001 C CNN
F 3 "" H 3450 2550 50  0000 C CNN
	1    3450 2550
	-1   0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 58A37574
P 5200 4250
F 0 "R3" V 5280 4250 50  0000 C CNN
F 1 "120K" V 5200 4250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5130 4250 50  0001 C CNN
F 3 "" H 5200 4250 50  0000 C CNN
	1    5200 4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 58A375E1
P 5200 5600
F 0 "#PWR01" H 5200 5350 50  0001 C CNN
F 1 "GND" H 5200 5450 50  0000 C CNN
F 2 "" H 5200 5600 50  0000 C CNN
F 3 "" H 5200 5600 50  0000 C CNN
	1    5200 5600
	1    0    0    -1  
$EndComp
$Comp
L CP C1
U 1 1 58A37754
P 6500 3900
F 0 "C1" H 6525 4000 50  0000 L CNN
F 1 "10µF 25V tantal" H 6150 4100 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Axial_D6_L11_P18" H 6538 3750 50  0001 C CNN
F 3 "" H 6500 3900 50  0000 C CNN
	1    6500 3900
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 58A37806
P 7500 2550
F 0 "P2" H 7500 2700 50  0000 C CNN
F 1 "OUTPUT" V 7600 2550 50  0000 C CNN
F 2 "myConnectors:JST-B2P-VH" H 7500 2550 50  0001 C CNN
F 3 "" H 7500 2550 50  0000 C CNN
	1    7500 2550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 58A3799A
P 7200 2700
F 0 "#PWR02" H 7200 2450 50  0001 C CNN
F 1 "GND" H 7200 2550 50  0000 C CNN
F 2 "" H 7200 2700 50  0000 C CNN
F 3 "" H 7200 2700 50  0000 C CNN
	1    7200 2700
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 58F350DF
P 6800 3900
F 0 "R11" V 6800 3900 50  0000 C CNN
F 1 "1K 1W" V 6900 3900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM20mm" V 6730 3900 50  0001 C CNN
F 3 "" H 6800 3900 50  0000 C CNN
	1    6800 3900
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 58F35B0E
P 4950 4700
F 0 "C2" H 4975 4800 50  0000 L CNN
F 1 "100nF" H 4850 4600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 4988 4550 50  0001 C CNN
F 3 "" H 4950 4700 50  0000 C CNN
	1    4950 4700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 58F360EB
P 3800 3100
F 0 "#PWR03" H 3800 2850 50  0001 C CNN
F 1 "GND" H 3800 2950 50  0000 C CNN
F 2 "" H 3800 3100 50  0000 C CNN
F 3 "" H 3800 3100 50  0000 C CNN
	1    3800 3100
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 59C67DAE
P 4450 4950
F 0 "R1" V 4530 4950 50  0000 C CNN
F 1 "8.2K" V 4450 4950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4380 4950 50  0001 C CNN
F 3 "" H 4450 4950 50  0000 C CNN
	1    4450 4950
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 59C68601
P 3800 2850
F 0 "C3" H 3825 2950 50  0000 L CNN
F 1 "1µF MLCC X7R" H 3500 2750 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 3838 2700 50  0001 C CNN
F 3 "" H 3800 2850 50  0000 C CNN
	1    3800 2850
	1    0    0    -1  
$EndComp
$Comp
L +20V #PWR04
U 1 1 59C70D05
P 4100 2400
F 0 "#PWR04" H 4100 2250 50  0001 C CNN
F 1 "+20V" H 4100 2550 50  0000 C CNN
F 2 "" H 4100 2400 50  0000 C CNN
F 3 "" H 4100 2400 50  0000 C CNN
	1    4100 2400
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 59C72097
P 4700 4350
F 0 "R5" V 4780 4350 50  0000 C CNN
F 1 "1K" V 4700 4350 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4630 4350 50  0001 C CNN
F 3 "" H 4700 4350 50  0000 C CNN
	1    4700 4350
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 59C7216C
P 4700 4950
F 0 "R6" V 4780 4950 50  0000 C CNN
F 1 "1K" V 4700 4950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4630 4950 50  0001 C CNN
F 3 "" H 4700 4950 50  0000 C CNN
	1    4700 4950
	1    0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 59C726C2
P 7100 3750
F 0 "R13" V 7180 3750 50  0000 C CNN
F 1 "6.8K" V 7100 3750 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7030 3750 50  0001 C CNN
F 3 "" H 7100 3750 50  0000 C CNN
	1    7100 3750
	1    0    0    -1  
$EndComp
$Comp
L R R14
U 1 1 59C727E8
P 7100 4250
F 0 "R14" V 7180 4250 50  0000 C CNN
F 1 "1K" V 7100 4250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7030 4250 50  0001 C CNN
F 3 "" H 7100 4250 50  0000 C CNN
	1    7100 4250
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 59C72C0E
P 4200 4950
F 0 "R2" V 4280 4950 50  0000 C CNN
F 1 "15K" V 4200 4950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4130 4950 50  0001 C CNN
F 3 "" H 4200 4950 50  0000 C CNN
	1    4200 4950
	1    0    0    -1  
$EndComp
$Comp
L LT3081 U1
U 1 1 59C64814
P 5100 2800
F 0 "U1" H 5100 2950 50  0000 C CNN
F 1 "LT3081" H 5100 2800 50  0000 C CNN
F 2 "library:TO-220_7" H 5100 2800 50  0001 C CNN
F 3 "" H 5100 2800 50  0000 C CNN
	1    5100 2800
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 59D2C0EA
P 5850 4500
F 0 "R7" V 5930 4500 50  0000 C CNN
F 1 "8.2K" V 5850 4500 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5780 4500 50  0001 C CNN
F 3 "" H 5850 4500 50  0000 C CNN
	1    5850 4500
	0    1    1    0   
$EndComp
$Comp
L CONN_01X06 P3
U 1 1 59D463BC
P 3450 4550
F 0 "P3" H 3450 4900 50  0000 C CNN
F 1 "MULTIMETER" V 3550 4550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 3450 4550 50  0001 C CNN
F 3 "" H 3450 4550 50  0000 C CNN
	1    3450 4550
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 59D47123
P 3750 5000
F 0 "#PWR05" H 3750 4750 50  0001 C CNN
F 1 "GND" H 3750 4850 50  0000 C CNN
F 2 "" H 3750 5000 50  0000 C CNN
F 3 "" H 3750 5000 50  0000 C CNN
	1    3750 5000
	1    0    0    -1  
$EndComp
$Comp
L +20V #PWR06
U 1 1 59D471D3
P 3700 4200
F 0 "#PWR06" H 3700 4050 50  0001 C CNN
F 1 "+20V" H 3700 4350 50  0000 C CNN
F 2 "" H 3700 4200 50  0000 C CNN
F 3 "" H 3700 4200 50  0000 C CNN
	1    3700 4200
	1    0    0    -1  
$EndComp
$Comp
L POT RV2
U 1 1 59D536C1
P 5200 5150
F 0 "RV2" H 5200 5070 50  0000 C CNN
F 1 "20K" H 5200 5150 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps-RK16-single" H 5200 5150 50  0001 C CNN
F 3 "" H 5200 5150 50  0000 C CNN
	1    5200 5150
	0    1    -1   0   
$EndComp
$Comp
L POT RV1
U 1 1 59D53980
P 5200 4750
F 0 "RV1" H 5200 4670 50  0000 C CNN
F 1 "2K" H 5200 4750 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps-RK16-single" H 5200 4750 50  0001 C CNN
F 3 "" H 5200 4750 50  0000 C CNN
	1    5200 4750
	0    1    -1   0   
$EndComp
$Comp
L POT RV3
U 1 1 59D53E8D
P 5600 4950
F 0 "RV3" H 5600 4870 50  0000 C CNN
F 1 "50K" H 5600 4950 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Bourns_3296W_3-8Zoll_Inline_ScrewUp" H 5600 4950 50  0001 C CNN
F 3 "" H 5600 4950 50  0000 C CNN
	1    5600 4950
	0    1    -1   0   
$EndComp
$Comp
L POT RV5
U 1 1 59D5410A
P 5800 3950
F 0 "RV5" H 5800 3870 50  0000 C CNN
F 1 "500K" H 5800 3950 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Bourns_3296W_3-8Zoll_Inline_ScrewUp" H 5800 3950 50  0001 C CNN
F 3 "" H 5800 3950 50  0000 C CNN
	1    5800 3950
	1    0    0    1   
$EndComp
$Comp
L POT RV4
U 1 1 59D54267
P 5800 3600
F 0 "RV4" H 5800 3520 50  0000 C CNN
F 1 "10K" H 5800 3600 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps-RK16-single" H 5800 3600 50  0001 C CNN
F 3 "" H 5800 3600 50  0000 C CNN
	1    5800 3600
	-1   0    0    1   
$EndComp
Text GLabel 4100 4200 1    60   Input ~ 0
TEMP
Text GLabel 4300 4200 1    60   Input ~ 0
IMON
Text GLabel 4550 4200 1    60   Input ~ 0
ILIM
Text GLabel 3900 4200 1    60   Input ~ 0
VOUT
Connection ~ 6800 2500
Wire Wire Line
	6800 2500 6800 3750
Wire Wire Line
	4700 3950 5650 3950
Wire Wire Line
	5200 5300 5200 5600
Wire Wire Line
	6150 3950 5950 3950
Wire Wire Line
	3800 3000 3800 3100
Connection ~ 3800 2500
Wire Wire Line
	3800 2500 3800 2700
Connection ~ 4950 5400
Wire Wire Line
	4950 5400 4950 4850
Connection ~ 5200 4000
Wire Wire Line
	4950 4000 5200 4000
Wire Wire Line
	4950 4550 4950 4000
Connection ~ 6800 5400
Wire Wire Line
	6800 5400 6800 4050
Wire Wire Line
	3650 2600 4000 2600
Wire Wire Line
	4000 2600 4000 5400
Wire Wire Line
	5200 3500 5200 4100
Wire Wire Line
	5200 4400 5200 4600
Wire Wire Line
	6500 2500 6500 3750
Connection ~ 6500 2500
Wire Wire Line
	7300 2600 7200 2600
Wire Wire Line
	7200 2600 7200 2700
Connection ~ 5200 5400
Wire Wire Line
	3650 2500 4300 2500
Wire Wire Line
	4000 5400 7100 5400
Wire Wire Line
	5200 5400 4950 5400
Connection ~ 5450 3950
Connection ~ 4100 2500
Wire Wire Line
	4200 2900 4300 2900
Wire Wire Line
	5900 2500 7300 2500
Wire Wire Line
	6500 4050 6500 5400
Connection ~ 6500 5400
Wire Wire Line
	6150 2500 6150 4500
Connection ~ 6150 2500
Wire Wire Line
	4100 2400 4100 2500
Wire Wire Line
	4700 3950 4700 4200
Wire Wire Line
	7100 3600 7100 2500
Connection ~ 7100 2500
Wire Wire Line
	7100 3900 7100 4100
Wire Wire Line
	7100 5400 7100 4400
Wire Wire Line
	7100 4000 7300 4000
Connection ~ 7100 4000
Wire Wire Line
	4450 3900 4950 3900
Wire Wire Line
	4950 3900 4950 3500
Wire Wire Line
	5450 3500 5450 3950
Wire Wire Line
	6150 4500 6000 4500
Connection ~ 6150 3950
Wire Wire Line
	5200 4500 5700 4500
Connection ~ 5200 4500
Wire Wire Line
	4700 4500 4700 4800
Wire Wire Line
	4700 5100 4700 5400
Connection ~ 4700 5400
Wire Wire Line
	4450 3900 4450 4800
Wire Wire Line
	4200 2900 4200 4800
Wire Wire Line
	4200 5100 4200 5400
Connection ~ 4200 5400
Wire Wire Line
	4450 5400 4450 5100
Connection ~ 4450 5400
Wire Wire Line
	3650 4300 4200 4300
Connection ~ 4200 4300
Connection ~ 4450 4400
Wire Wire Line
	3900 4200 3900 5500
Wire Wire Line
	3900 5500 7300 5500
Wire Wire Line
	7300 5500 7300 4000
Wire Wire Line
	3750 5000 3750 4800
Wire Wire Line
	3750 4800 3650 4800
Wire Wire Line
	3700 4200 3700 4700
Wire Wire Line
	3700 4700 3650 4700
Wire Wire Line
	5200 4900 5200 5000
Wire Wire Line
	5350 5150 5400 5150
Wire Wire Line
	5350 4750 5400 4750
Wire Wire Line
	5750 4950 5800 4950
Wire Wire Line
	5800 4950 5800 4750
Wire Wire Line
	5800 4750 5600 4750
Wire Wire Line
	5600 4500 5600 4800
Connection ~ 5600 4500
Connection ~ 5600 4750
Wire Wire Line
	5600 5100 5600 5400
Connection ~ 5600 5400
Wire Wire Line
	5800 4100 5800 4150
Wire Wire Line
	5800 4150 6000 4150
Wire Wire Line
	6000 4150 6000 3950
Connection ~ 6000 3950
Wire Wire Line
	5650 3600 5450 3600
Connection ~ 5450 3600
Wire Wire Line
	5950 3600 6150 3600
Connection ~ 6150 3600
Wire Wire Line
	5800 3750 5800 3800
Wire Wire Line
	5800 3800 6000 3800
Wire Wire Line
	6000 3800 6000 3600
Connection ~ 6000 3600
Wire Wire Line
	5400 5150 5400 5400
Connection ~ 5400 5400
Wire Wire Line
	5400 4750 5400 4950
Wire Wire Line
	5400 4950 5200 4950
Connection ~ 5200 4950
Wire Wire Line
	3650 4600 4700 4600
Connection ~ 4700 4600
Wire Wire Line
	3900 4500 3650 4500
Wire Wire Line
	3650 4400 4450 4400
Wire Wire Line
	4100 4200 4100 4300
Connection ~ 4100 4300
Wire Wire Line
	4300 4200 4300 4400
Connection ~ 4300 4400
Wire Wire Line
	4550 4200 4550 4600
Connection ~ 4550 4600
Connection ~ 3900 4500
$EndSCHEMATC
