# LABORATORY DUAL 6-18V POWER SUPPLY

The purpose of this page is to explain step by step the realization of a dual laboratory power supply delivering 6V to 18V.

This is a very low noise power supply and it is intended to power high-end audio electronics.

It can provide 1.5 amps

It has the following settings:
 * main setting output voltage
 * fine adjustment of the output voltage
 * maximum output current

It is of course protected against short circuits.

It has a mains filter.

It is equipped with two 2-line displays to display the output voltage, the output current and the temperature of each regulator. These two multimeters are each managed by an ARDUINO NANO.

Finally, in addition to the ON / OFF switch and its blue LED it has a switch to enable / disable the output. A red LED indicates that the output is active.

The boards use the following components :

 * 2 LT3081 regulators
 * 2 ARDUINO NANO
 * 2 LCD displays
 * a toroïdal transformer 2x12V 50VA
 * a lot of passive components

### ELECTRONICS

The schema is made using KICAD.

In the kicad/regulator-LT3081 directory the two files regulator-LT3081-left.net and regulator-LT3081-left.sch are symbolic links to regulator-LT3081.net and regulator-LT3081.sch

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/02/une-alimentation-symetrique-6-18v.html
