#include <LiquidCrystal.h>

//#define DEBUG
#define NSAMPLE     1000
#define REFERENCE   2.428       // RIGHT reference
//#define REFERENCE   2.473       // LEFT reference
#define V_DIVIDER   7.87      // RIGHT divider bridge 6.8K | 1K
//#define V_DIVIDER   7.895     // LEFT divider bridge 6.8K | 1K
#define SAMP_DELAY  0

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  Serial.begin(115200);
  lcd.begin(16, 2);
  Serial.println("POWER-SUPPLY MULTIMETER");
  analogReference(EXTERNAL);
}

void displayVoltage(void)
{
  long value = 0;
  for (int i = 0 ; i < NSAMPLE ; i++) {
    value += analogRead(1);
    delay(SAMP_DELAY);
  }
  value /= NSAMPLE;
  float voltage = value * REFERENCE / 1023.0 * V_DIVIDER; 
#ifdef DEBUG
  Serial.print("ADC V: ");
  Serial.print(value);
  Serial.print("                 voltage: ");
  Serial.println(voltage, 2);
#else
  Serial.print("voltage: ");
  Serial.print(voltage, 2);
#endif
  lcd.setCursor(0, 0);
  lcd.print("       ");
  lcd.setCursor(0, 0);
  lcd.print(voltage, 2);
  lcd.print("V");
}

void displayCurrent(void)
{
  long value = 0;
  for (int i = 0 ; i < NSAMPLE ; i++) {
    value += analogRead(2);
    delay(SAMP_DELAY);
  }
  value /= NSAMPLE;
  float voltage = (value * REFERENCE) / 1023.0;
  float current = (voltage / 8200 * 5000); // current / 5000 in 8.2K
#ifdef DEBUG
  Serial.print("ADC A: ");
  Serial.print(value);
  Serial.print(" A voltage: ");
  Serial.print(voltage, 2);
  Serial.print(" current: ");
  Serial.println(current, 3);
#else
  Serial.print(" current: ");
  Serial.println(current, 3);
#endif
  lcd.setCursor(11, 0);
  lcd.print("       ");
  lcd.setCursor(11, 0);
  lcd.print(current, 2);
  lcd.print("A");
}

void displayTemperature(void)
{
  long value = 0;
  for (int i = 0 ; i < NSAMPLE ; i++) {
    value += analogRead(3);
    delay(SAMP_DELAY);
  }
  value /= NSAMPLE;
  float voltage = (value * REFERENCE) / 1023.0;
  float temperature = voltage * 66; // 66 = 1µA / °C in 15K
#ifdef DEBUG
  Serial.print("ADC T: ");
  Serial.print(value);
  Serial.print(" T voltage: ");
  Serial.print(voltage, 2);
#endif
  Serial.print(" temp: ");
  Serial.println(temperature, 2);
  lcd.setCursor(6, 1);
  lcd.print("       ");
  lcd.setCursor(6, 1);
  lcd.print(temperature, 0);
  lcd.print(char(223));
  lcd.print("C");
}

void loop() {
  static int count = 50;
  displayVoltage();
  displayCurrent();
  if (count == 50) {
    displayTemperature();
    count = 0;
  }
  count++;
}

